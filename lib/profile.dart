import 'package:flutter/material.dart';

class StudentProfile extends StatelessWidget {
  const StudentProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: buildBodyProfileWidget(),
      //   child: ElevatedButton(
      //     onPressed: () {
      //       Navigator.pop(context);
      //     },
      //     child: const Text('Go back!'),
      //   ),

    );
  }
}

AppBar buildAppBar(BuildContext context){
  return AppBar(
    // backgroundColor: Colors.cyan[200],
    backgroundColor: Colors.pink[400],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined, //login_outlined, //arrow_back_ios_new_outlined,
          color: Colors.white,
        )
    ),
    title: Text("บัญชีผู้ใช้งาน"),
    actions: <Widget>[
      // IconButton(
      //     onPressed: (){},
      //     icon: Icon(
      //       Icons.print,
      //       color: Colors.white,
      //     )
      // ),
    ],
  );
}

Widget buildBodyProfileWidget(){
  return Container(
      padding: EdgeInsets.all(16),
      child: ListView(
        children: <Widget>[
          const SizedBox(
            height: 10,
          ),
          buildCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildAccountWidget(),
          const SizedBox(
            height: 10,
          ),
          buildStdCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildPersonalCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildOthPerCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildMoreDataCardWidget(),
          const SizedBox(
            height: 10,
          ),
        ],
      )
  ) ;
}

Widget buildCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.pinkAccent, width: 5),
          ),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 150,
                  height: 120,
                  child: Image.network(
                    "https://ichef.bbci.co.uk/news/800/cpsprodpb/1124F/production/_119932207_indifferentcatgettyimages.png",
                    fit: BoxFit.cover,
                  ),
                ),
                const SizedBox(
                  width: 15,
                ),
                Text('มหาวิทยาลัยบูรพา\n'
                    'Burapha University \n'
                    'คณะวิทยาการสารสนเทศ \n'
                    'Faculty of Informatics \n'
                    )
              ],
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildAccountWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.pinkAccent, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              // leading: Icon(Icons.circle_outlined, color: Colors.pink[400],),
              title: const Text(
                'บัญชีผู้ใช้งาน',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              color: Colors.pink[50],
              child: Column(
                children: [
                  Text(
                    '\n"12 Days"\n',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    '  จำนวนวันที่ใช้รหัสผ่านได้  \n',
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ), //S
            Text(
              'รหัสผ่านหมดอายุ: 18 สิงหาคม 2566 [23:59:00]\n',

              style: TextStyle(color: Colors.red[900]),
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
            SizedBox(
              width: 144,
              child: ElevatedButton(
                onPressed: () => 'Null',
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.pink[400])),
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text('เปลี่ยนรหัสผ่าน'),
                      //Icon(Icons.touch_app),
                      Icon(Icons.edit),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildStdCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.pinkAccent, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              // leading: Icon(Icons.circle_outlined, color: Colors.pink[400],),
              title: const Text(
                'ข้อมูลด้านการศึกษา',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
              // subtitle: Text(
              //   'Secondary Text',
              //   style: TextStyle(color: Colors.black.withOpacity(0.6)),
              // ),
            ),
            Text(
              '\nรหัสประจำตัว: 62160252\n'
                  'เลขที่บัตรประชาชน: 1250101475159\n'
                  'ชื่อ: นางสาวชนากานต์ พานิชกูล\n'
                  'ชื่ออังกฤษ: MISS CHANARKARN PARNICHAKUL\n'
                  'คณะ: คณะวิทยาการสารสนเทศ\n'
                  'วิทยาเขต: บางแสน\n'
                  'หลักสูตร: 2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ\n'
                  'วิชาโท: -\n'
                  'ระดับการศึกษา: ปริญญาตรี\n'
                  'ชื่อปริญญา: วิทยาศาสตรบัณฑิต วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ\n'
                  'ปีการศึกษาที่เข้า: 2562/1 (17/4/25632)\n'
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
            SizedBox(
              width: 128,
              child: ElevatedButton(
                onPressed: () => 'Null',
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.pink[400])),
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text('แก้ไขข้อมูล'),

                      Icon(Icons.edit),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildPersonalCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.pinkAccent, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              // leading: Icon(Icons.circle_outlined, color: Colors.pink[400],),
              title: const Text(
                'ข้อมูลส่วนบุคคล',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              '\nสัญชาติ: ไทย ศาสนา: พุทธ\n'
                  'หมู่เลือด: B\n'
                  'ชื่อมารดา: น.ส.สุภาพร แสงสมบุญ\n'
                  'ที่อยู่: 582 ม.3 ตำบล ศรีมหาโพธิ อำเภอ ศรีมหาโพธิ ปราจีนบุรี 25140\n'
                  'โทร: 0915848243\n'
                  'ผู้ปกครอง: น.ส.สุภาพร แสงสมบุญ\n'
                  'ที่อยู่ (ผู้ปกครอง): 582 ม.3 ตำบล ศรีมหาโพธิ อำเภอ ศรีมหาโพธิ ปราจีนบุรี 25140\n'
                  'โทร: 0936571928\n',
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
            SizedBox(
              width: 128,
              child: ElevatedButton(
                onPressed: () => 'Null',
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.pink[400])),
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text('แก้ไขข้อมูล'),

                      Icon(Icons.edit),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildOthPerCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.pinkAccent, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              // leading: Icon(Icons.circle_outlined, color: Colors.pink[400],),
              title: const Text(
                'บุคคลที่สามารถติดต่อได้',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              '\nชื่อ: นางสาวชนากานต์ พานิชกูล\n'
                  'ที่อยู่: 582 ม.3 ต.ศรีมหาโพธิ อ.ศรีมหาโพธิ จ.ปราจีนบุรี 25140\n'
                  'โทร: 0915848243\n',
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
            SizedBox(
              width: 128,
              child: ElevatedButton(
                onPressed: () => 'Null',
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.pink[400])),
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text('แก้ไขข้อมูล'),

                      Icon(Icons.edit),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildMoreDataCardWidget(){
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.pinkAccent, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              // leading: Icon(Icons.circle_outlined, color: Colors.pink[400],),
              title: const Text(
                'ข้อมูลเพิ่มเติมอื่นๆ',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              '\nความพิการ: ไม่พิการ'
                  'สถานภาพการรับทุน: ไม่ได้รับทุน'
                  'ความต้องการทุนการศึกษา: ต้องการ\n'
                  'จำนวนพี่น้องทั้งหมด (รวมตัวเอง): 2\n'
                  'นิสิตเป็นบุตรคนที่: 1\n'
                  'จำนวนพี่น้องที่กำลังศึกษาอยู่(รวมตัวเอง): 2\n'
                  'สถานภาพของมารดา: มีชีวิต\n'
                  'รายได้มารดา: < 150,000 บาทต่อปี (< 12,500 บาทต่อเดือน)\n'
                  'อาชีพมารดา: ค้าขาย\n'
                  'วุฒิการศึกษาสูงสุดของมารดา: ปวช.\n'
                  'สถานภาพของมารดา: มีชีวิต\n'
                  'สภาพการสมรสของบิดามารดา: หย่าร้าง\n'
                  'รายได้ผู้ปกครอง: < 150,000 บาทต่อปี (< 12,500 บาทต่อเดือน)\n'
                  'อาชีพผู้ปกครอง: ค้าขาย\n',
            ),
            const SizedBox(
              height: 10,
            ), //SizedBox
            SizedBox(
              width: 128,
              child: ElevatedButton(
                onPressed: () => 'Null',
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.pink[400])),
                child: Padding(
                  padding: const EdgeInsets.all(2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Text('แก้ไขข้อมูล'),

                      Icon(Icons.edit),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}