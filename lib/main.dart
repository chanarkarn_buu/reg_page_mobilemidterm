import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'main_page_reg.dart';
import 'profile.dart';

void main() {
  runApp(const RegApp());
}

class RegApp extends StatelessWidget {
  const RegApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) =>
          MaterialApp(
            debugShowCheckedModeBanner: false,
            useInheritedMediaQuery: true,
            builder: DevicePreview.appBuilder,
            locale: DevicePreview.locale(context),
            title: 'Responsive and adaptive UI in Flutter',
            theme: ThemeData(
              primarySwatch: Colors.pink,
            ),
            home: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                title: const Text('REG BUU'),
              ),
              // drawer: Container(
              //   width: 250,
              //   color: Colors.red.shade200,
              //   // margin: EdgeInsets.only(top: 15, bottom: 8),
              //   child: Row(
              //    // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //    children: <Widget>[
              //       buildTableViewButton(),
              //
              //     ],
              //    ),
              // ),
              drawer: Drawer(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    UserAccountsDrawerHeader(
                      accountName: Text('Chanarkarn Parnichakul'),
                      accountEmail: Text('62160252@go.buu.ac.th'),
                      currentAccountPicture: CircleAvatar(
                        child: Text("C"),
                        foregroundColor: Colors.white,
                        backgroundColor: Colors.purpleAccent.shade100,
                      ),
                      otherAccountsPictures: <Widget>[

                      ],
                    ),
                    ListTile(
                        title: Text('ประวัตินิสิต'),
                         onTap: () {
                          Navigator.push(
                            context,
                             MaterialPageRoute(builder: (context) => StudentProfile()),
                          );
                         },
                    ),
                    Divider(),
                    ListTile(
                      title: Text('ตรวจสอบวันจบการศึกษา'),
                      onTap: () {},
                    ),
                    Divider(),
                    ListTile(
                      title: Text('ตารางเรียน/สอบ'),
                      onTap: () {},
                    ),
                    Divider(),
                    ListTile(
                      title: Text('ปฎิทินการศึกษา'),
                      onTap: () {},
                    ),
                    Divider(),
                    ListTile(
                      title: Text('ติดต่อเรา'),
                      onTap: () {},
                    ),
                    Divider(),
                    ListTile(
                      title: Text('วิชาที่เปิดสอน'),
                      onTap: () {},
                    ),
                    Divider(),
                    ListTile(
                      title: Text('ระเบียบข้อบังคับ'),
                      onTap: () {},
                    ),
                    Divider(),
                    ListTile(
                      title: Text('ออกจากระบบ'),
                      onTap: () {},
                    ),
                    Divider(),
                    // Expanded(
                    //   child: Align(
                    //     alignment: Alignment.bottomLeft,
                    //     child: ListTile(
                    //       title: Text('ออกจากระบบ'),
                    //       onTap: () {},
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
              ),
              body: const ContactProfilePage(),
            ),
          ),
    );
  }
}

// Widget buildTableViewButton() {
//   return Column(
//     children: <Widget>[
//       IconButton(
//         icon: Icon(
//           Icons.table_view  ,
//           color: Colors.pink.shade300,
//         ),
//         onPressed: () {},
//       ),
//       Text("ปฎิทินการศึกษา"),
//     ],
//   );
// }
