import 'package:flutter/material.dart';
import 'profile.dart';
import 'TableStudent.dart';

void main() {
  runApp(ContactProfilePage());
}

class ContactProfilePage extends StatelessWidget {
  const ContactProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  //Height constraint at Container widget level
                  height: 180,
                  child: Image.network(
                    "https://upload.wikimedia.org/wikipedia/th/thumb/2/27/Buu0001.jpeg/420px-Buu0001.jpeg",
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  height: 60,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text("ประกาศสำคัญ!!",
                          style: TextStyle(color: Colors.pinkAccent, fontSize:30),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 40,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text("กำหนดการชำระค่าบำรุงหอพักภาคฤดูร้อน "
                            "ปีการศึกษา 2565 ",
                          style: const TextStyle(fontSize:13),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 450,
                  //Height constraint at Container widget level
                  height: 250,
                  child: Image.network("https://i0.wp.com/www.sakaeo.buu.ac.th/2017/wp-content/uploads/2023/01/%E0%B8%87%E0%B8%B2%E0%B8%99%E0%B8%81%E0%B8%B4%E0%B8%88%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%99%E0%B8%B4%E0%B8%AA%E0%B8%B4%E0%B8%95-16012566-03.png?resize=450%2C250&ssl=1",
                    errorBuilder: (context,url, error) => Icon(Icons.error)
                  )
                ),
                Divider(
                  color: Colors.grey,
                ),

                Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      buildProfileButton(context),
                      buildCalendarButton(),
                      buildTableButton(context),

                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 8, bottom: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      buildMessageButton(),
                      buildTableViewButton(),
                      buildRecentButton(),

                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

Widget buildProfileButton(BuildContext context) {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.account_box_rounded ,
          color: Colors.pink.shade300,
        ),
        onPressed: () {
        Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => StudentProfile()),
           );
         },

      ),
      Text("บัญชีผู้ใช้งาน"),
    ],
  );
}

Widget buildCalendarButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.edit_calendar_rounded ,
          color: Colors.pink.shade300,
        ),
        onPressed: () {},
      ),
      Text("ตรวจสอบวันจบ"
          "การศึกษา"),
    ],
  );
}

Widget buildTableButton(BuildContext context) {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.list_alt_outlined ,
          color: Colors.pink.shade300,
        ),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => StudentTable()),
          );
        },
      ),
      Text("ตารางเรียน/สอบ"),
    ],
  );
}

Widget buildMessageButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message ,
          color: Colors.pink.shade300,
        ),
        onPressed: () {},
      ),
      Text("ภาระค่าใช้จ่ายทุน"),
    ],
  );
}

Widget buildRecentButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.recent_actors ,
          color: Colors.pink.shade300,
        ),
        onPressed: () {},
      ),
      Text("หลักสูตรที่เปิดสอน"),
    ],
  );
}

Widget buildTableViewButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.table_view  ,
          color: Colors.pink.shade300,
        ),
        onPressed: () {},
      ),
      Text("ปฎิทินการศึกษา"),
    ],
  );
}